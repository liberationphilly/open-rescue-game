print("What's your name?")
name = input("--> ")

print("Hello, " + name + "!")

print("We're going to need to see some ID to let you in. What's the password?")
password = input("--> ")

if password == "269":
    print("Welcome, comrade.")
else:
    exit("Only members may pass!")

print()
print("===============================")
print("WELCOME TO OPEN RESCUE THE GAME")
print("by Liberation Philadelphia     ")
print("===============================")
print()

print("You awake this morning with a pang of dissatisfaction. What shall you do?")
print("1) Sleep it out")
print("2) Rescue animals from a slaughterhouse")
choice = input("--> ")

if choice == "1":
    print("You drift into a deep sleep and expunge the worries of the world from your soul.")
    exit("THE END")
elif choice == "2":
    print("It is night time. A winding dirt path leads to the back door of a slaughterhouse. Guard dogs loom in the area. How do you pass?")
    print("1) Toss treats to the dogs. Good boys!")
    print("2) Sprint past the dogs")
else:
    exit("Invalid choice, sorry!")

choice = input("--> ")

if choice == "1":
    print("The dogs wagged their tails and you pet them. They are very good boys. You proceed through the slaughterhouse back door, which is conveniently unlocked.")
    print("Inside there are hundreds of confined pigs. The conditions are horrible. You think you can save one. What do you do?")
    print("1) Nah, that sounds too hard.")
    print("2) Find an injured piglet who needs your help most and rush them out of there. Take photos of what you saw.")
    print("3) Go home, but commit to making incremental legislative changes that will improve animal welfare over the course of several generations.")
elif choice == "2":
    print("You tried to make a run for it, but those viscious attack dogs took you down! Better luck next time.")
    exit("THE END")
else:
    exit("Invalid choice.")

choice = input("--> ")

if choice == "1":
    print("You went home and ate an entire tub of vegan Ben & Jerry's alone. There's really no point to anything, anyway.")
    exit("THE END")
elif choice == "2":
    print("You rushed that piglet out of the scene! Sweet liberation!")
elif choice == "3":
    print("You made an important commitment to the animals that day, but that memory always haunted you and you wished you'd been stronger.")
    exit("THE END")

print("What will you name her?")
piglet = input("--> ")
print("You rush " + piglet + " home and give her water and food. She perks up a bit. You find a 24 hour vet and take her in. She is weak but recovering.")
print(piglet + " is now your responsiblity and you're faced with finding her forever home. What shall you do?")
print("1) Adopt her yourself")
print("2) Contact local animal sanctuaries")
choice = input("--> ")

if choice == "1":
    print("You and " + piglet + " have an unbreakable bond. You adopt her into your family and remain friends forever!")
    print("Good job! THANK YOU FOR PLAYING!!")
elif choice == "2":
    print("A local sanctuary says they can take in " + piglet + ". You research the place, visit it, and reluctantly release your friend to them.")
    print("You are sad to see her go, but " + piglet + " will now have the opportunity to live happy and free, playing with other piglets and feeling the grass beneath her feet!")
    print("Good job! THANK YOU FOR PLAYING!!")
else:
    exit("Invalid choice")

print()
print("This game was made by members of Liberation Philadelphia. It's licensed under the GNU GPL-3.0")
print("You are free to view, modify, and sell this game's code, as long as you grant others these freedoms that we granted to you.")
