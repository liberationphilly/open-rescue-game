# Open Rescue [[ THE GAME ]]

Open Rescue THE GAME is a text-based adventure game created by members of Liberation Philly Tech. It's written in Python.

As an introduction to programming, members will follow along to create the simple program included in this repo. Later, this repo will serve as an introduction to git and GitHub where members will create pull requests against the repo to add content to the game.

## Running the game

1. Install Python 3 and git on your computer.
2. Clone this repo by typing `git clone https://github.com/liberationphilly/open-rescue-game.git` into your terminal.
3. Change into the repo's folder by writing `cd open-rescue-game` in your terminal.
4. Finally, type `python3 adventure.py` in your terminal to play.
5. Optional: You may quit the game while it's running by pressing `Ctrl + C` on your keyboard.

## Legal

The code of this project is licensed under the GPL-3.0. By creating a pull request or otherwise contributing code to this project, you implicitly agree to license your code under the GPL-3.0.
